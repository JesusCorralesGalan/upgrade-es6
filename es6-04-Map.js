// 4.1 Dado el siguiente array, devuelve un array con sus nombres 
// utilizando .map().
const users = [
    { id: 1, name: 'Abel' },
    { id: 2, name: 'Julia' },
    { id: 3, name: 'Pedro' },
    { id: 4, name: 'Amanda' }
];

const result1 = users.map(item => item.name);
console.log(result1);


// 4.2 Dado el siguiente array, devuelve una lista que contenga los valores 
// de la propiedad .name y cambia el nombre a 'Anacleto' en caso de que 
// empiece por 'A'.
const users2 = [
    { id: 1, name: 'Abel' },
    { id: 2, name: 'Julia' },
    { id: 3, name: 'Pedro' },
    { id: 4, name: 'Amanda' }
];
// const sobreNombre = 'Anacleto';
// const result2 = users2.map(item =>(
//  if (item.includes('A')){
//      item=sobreNombre
//      });    
// );

// for (let q = 0; q < result1.length; q++) {
//     const item = result1[q];
//     const sobreNombre = 'Anacleto'
//     if (item.includes('A')) { result1[q] = sobreNombre }

// }
// console.log(result2);

//var result2 = users2.map(person => ({ value: person.id, text: person.name }));

let result2 = users2.map(person => {
    if (person.name === 'Amanda') {

    }
});



console.log(result2)

// 4.3 Dado el siguiente array, devuelve una lista que contenga los valores 
// de la propiedad .name y añade al valor de .name el string ' (Visitado)' 
// cuando el valor de la propiedad isVisited = true.
const cities = [
    { isVisited: true, name: 'Tokyo' },
    { isVisited: false, name: 'Madagascar' },
    { isVisited: true, name: 'Amsterdam' },
    { isVisited: false, name: 'Seul' }
];
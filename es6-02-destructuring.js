// 2.1 En base al siguiente javascript, crea variables en base a las propiedades 
// del objeto usando object destructuring e imprimelas por consola. Cuidado, 
// no hace falta hacer destructuring del array, solo del objeto.

const game = {title: 'The last us 2', gender: ['action', 'zombie', 'survival'], year: 2020}


let { title} = game; 
let {gender} = game; 
let {year} = game; 
console.log(title, gender, year);

console.log(title);
console.log(gender);
console.log(year);


// 2.2 En base al siguiente javascript, usa destructuring para crear 3 variables 
// llamadas fruit1, fruit2 y fruit3, con los valores del array. Posteriormente
// imprimelo por consola.

const fruits = ['Banana', 'Strawberry', 'Orange'];

const [fruit1, fruit2, fruit3] = fruits


console.log(fruit1, fruit2, fruit3);
// 2.3 En base al siguiente javascript, usa destructuring para crear 2 
// variables igualandolo a la función e imprimiendolo por consola.

const animalFunction = () => {
    return {name: 'Bengal Tiger', race: 'Tiger'}
};

let {name} = animalFunction();
let {race} = animalFunction();

console.log(race, name);


// 2.4 En base al siguiente javascript, usa destructuring para crear las 
// variables name y itv con sus respectivos valores. Posteriormente crea 
// 3 variables usando igualmente el destructuring para cada uno de los años 
// y comprueba que todo esta bien imprimiendolo.



const car = {version: 'Mazda 6', itv: [2015, 2011, 2020] };
//cambio el key 'name' por 'version' por un conflicto con variables anteriores
let {version} = car;
let {itv}=car;
console.log(version, itv);

 let [añoUno, añoDos, añoTres] = itv;

console.log(añoDos, añoUno, añoTres);
